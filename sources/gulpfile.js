var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglifyjs'),
	cssnano = require('gulp-cssnano'),
	rename = require('gulp-rename'),
	imagemin     = require('gulp-imagemin'), 
	pngquant     = require('imagemin-pngquant'),
	cache        = require('gulp-cache'),
	autoprefixer = require('gulp-autoprefixer');
	
gulp.task('sass', function() {
	return gulp.src('app/sass/main.sass')
		.pipe(sass())
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
		.pipe(gulp.dest('app/css/'))
		.pipe(browserSync.reload({stream: true}))
});



gulp.task('browser-sync', function () {
	browserSync({
		server:{
			baseDir: 'app'
		},
		notify: false
	});
});

gulp.task('scripts', function() {
	return gulp.src([
		'app/libs/bootstrap.bundle.min.js',
		'app/libs/bootstrap.min.js',
		'app/libs/wow.js',
		'app/libs/slick.min.js',
		'app/js/main.js'
		])
	.pipe(concat('scripts.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('dist/scripts'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('css-libs', ['sass'], function() {
	return gulp.src([
		'app/libs/bootstrap.min.css',
		'app/libs/font-awesome.min.css',
		'app/libs/slick.css',
		'app/libs/slick-theme.css',
		'app/libs/animate.min.css',
		'app/css/main.css' 
		]) 
		.pipe(concat('style.min.css'))
		.pipe(cssnano()) 
		.pipe(gulp.dest('dist/css'));
});

gulp.task('watch',['browser-sync','sass'], function () {
	gulp.watch('app/sass/*.sass', ['sass']);
	gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/scripts/*.js', browserSync.reload);
});


gulp.task('img', function() {
	return gulp.src('app/img/**/*') 
		.pipe(cache(imagemin({ 
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		})))
		.pipe(gulp.dest('dist/img'));
});

gulp.task('build',['sass','scripts','css-libs','img'], function () {


	var buildFonts = gulp.src('app/fonts/**/*')
		.pipe(gulp.dest('dist/fonts'));

	var buildjq = gulp.src('app/libs/jquery-3.2.1.slim.min.js') 
		.pipe(gulp.dest('dist/scripts'))

	var buildHtml = gulp.src('app/*.html') 
		.pipe(gulp.dest('dist'));
});
