$('.single-item').slick({
  infinite: true,
  slidesToShow: 1,
  prevArrow: '<img class="prev" src="img/previous.png" alt="">',
  nextArrow: '<img class="next" src="img/next.png" alt="">',
  slidesToScroll: 1
});


(function($) {
  $(function() {
    $('.toggle-overlay').click(function() {
      $('aside').toggleClass('open');
    });
  });
})(jQuery);